<?php
namespace app\index\Controller;

use think\Controller;
use think\Db;
use think\facade\Cache;

class Api extends Controller
{
    public static $uid;
    
    public function __construct(){
        Api::$uid = isset($_SERVER["HTTP_UID"]) ? $_SERVER["HTTP_UID"] : "";
    }
    
    //  获取地区
    public function getRegion(){
        if ( !cache("regionAll") ){
            $region = Db::name("region")->select();
            cache("regionAll",$region);
            return json(["code"=>200,"msg"=>"success","data"=>$region]);
        }else{
            return json(["code"=>200,"msg"=>"success","data"=>cache("regionAll")]);
        }
        return json(["code"=>500,"msg"=>"服务器繁忙!"]);
    }
    
    //  获取所有省份地区
    public function getProvince(){
        if ( !cache("regionProvince") ){
            $region = Db::name("region")->where("level",1)->select();
            cache("regionProvince",$region);
            return json(["code"=>200,"msg"=>"success","data"=>$region]);
        }else{
            return json(["code"=>200,"msg"=>"success","data"=>cache("regionProvince")]);
        }
        return json(["code"=>500,"msg"=>"服务器繁忙!"]);
    }
    
    //  获取下级地区
    public function getInferior($pid){
        $region = Db::name("region")->where("pid",$pid)->select();
        if ($region){
            return json(["code"=>200,"msg"=>"success","data"=>$region]);
        }
        return json(["code"=>500,"msg"=>"服务器繁忙!"]);
    }
}
?>