<?php
namespace app\index\model;

use think\Model;
use think\facade\Config;
use think\Db;

class Uploadfile extends Model
{
    
    //  上传文件限制大小
    // public $file_upload_size = Config("app.file_upload_size");
    
    //  上传文件路径
    // protected $file_upload_size = Config("app.file_upload_path");
    
    
    //  上传文件处理
    public function upFile($file,$channel=10,$file_type=10,$uploader_id=0){
        
        $info = $file->validate(['size'=>Config("app.file_upload_size")*1024*1024,'ext'=>Config("app.file_upload_type")])->move("./uploads/10001/");
        if ( $info ){
            $fileinfo = array();
            $fileinfo["file_path"] = "10001/".$info->getSaveName();
            $fileinfo["file_name"] = $info->getFilename();
            $fileinfo["file_ext"] = $info->getExtension();
            $fileinfo["file_type"] =  $file_type;
            $fileinfo["uploader_id"] = $uploader_id;
            $fileinfo["channel"] = $channel;
            $fileinfo["storage"] = "local";
            $fileinfo["store_id"] = 10001;
            $fileinfo["create_time"] = time();
            $fileinfo["update_time"] = time();
            
            return Db::name("upload_file")->insertGetId($fileinfo);
            
        }else{
            return 0;
        }
    }
}

?>