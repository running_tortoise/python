<?php
/*
 * @Descripttion: 
 * @version: 1.0.0
 * @Author: 奔跑的乌龟
 * @Date: 2021-10-16 18:46:00
 * @LastEditors: 奔跑的乌龟
 * @LastEditTime: 2021-10-25 15:39:04
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::header('Access-Control-Allow-Headers', 'Operator_id,Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With,token,uid')//允许自定义标头 Operator_id
    ->allowCrossDomain();//路由全局允许跨域

Route::post('register','index/user/register')->allowCrossDomain(); //  注册
Route::post('login','index/user/login')->allowCrossDomain();       //  账号登陆

Route::post('getopenid/:code','index/user/getWxId')->allowCrossDomain();       //  微信小程序登陆
Route::post('wxlogin/:code','index/user/wxlogin')->allowCrossDomain();       //  微信小程序登陆


//Route::get('/', 'index/index/index');

//  miss路由
Route::miss(function(){
    return json(["code"=>404,"msg"=>"api not found!"]); 
});   
/**
 * 用户路由分组
 */

//  书籍
Route::group('book', function () {

    //  获取书本列表
    Route::get('list/:page', 'index/book/list');
    //  获取单个书籍信息
    Route::get('getbook/:bid','index/book/getbook');
    //  获取章节
    Route::get('getchapter/:bid', 'index/chapter/getchapter');
    //  获取章节内容
    Route::get('getchaptercontent/:cid', 'index/chapter/getChapterContent');
    //  模糊搜索图书
    Route::get('searchbook/:name', 'index/book/searchBook/');
    //  获取用户对书籍的状态
    Route::get('getStatus/:uid/:bid', 'index/book/getUserBookStatus');
    //  获取分类列表
    Route::get('getclassify', 'index/book/getClassify');
    //  获取分类图书
    Route::get('getclassbook/:tid', 'index/book/getClassBook');
    //  获取榜单图书
    Route::get('topbooks/:page', 'index/book/topBooks');
    //  获取热门分类
    Route::get('gethotclassify', 'index/book/getHotClassify');
    //  获取最新书籍10
    Route::get('newbooks/:page', 'index/book/newBooks');
    //  获取最新书籍10 
    Route::get('monthtopbook/:page', 'index/book/monthTopBook');
    //  获取最新书籍10 
    Route::get('weektopbook/:page', 'index/book/weekTopBook');
    //  获取最新书籍10 
    Route::get('toplikebook/:page', 'index/book/topLikeBook');

})->middleware('apiMiddleware');

//  需要用户登陆
Route::group('user', function (){
    //  收藏书籍
    Route::get('callectbook', 'index/book/callectbook');
    //  喜欢书籍
    Route::get('likebook/:uid/:bid', 'index/book/likebook');
    //  获取收藏列表
    Route::get('getcollect/:uid', 'index/book/getCollect');
    //  搜索收藏列表
    Route::get('searchcollectbook/:uid/:name', 'index/book/searchCollectBook');
    
})->middleware('verifyLogin');


//  章节
Route::group('chapter', function () {

    //  获取章节
    Route::get('getchapter/:bid', 'index/chapter/getchapter');
    //  获取章节内容
    Route::get('getchaptercontent/:cid', 'index/chapter/getChapterContent');

})->middleware('apiMiddleware');

return [

];
