<?php

namespace app\http\middleware;

class verifyLogin
{
    public function handle($request, \Closure $next)
    {
        
        $clientToken =  $request->header('token');
            
        $uid = $request->header('uid');
        
        $serverToken = Cache($uid."LOGIN_TOKEN");
        
        //  验证用户是否登录
        if ( $serverToken==false ){
            return json([
                'code'  =>  8,
                'msg'   =>  '用户未登录!',
                'stoken'    => $serverToken,
                'uid'   =>  $uid
            ]);
        }
        // return json($clientToken);
        //  验证登录是否过期
        if ( $serverToken!=$clientToken ){
            return json([
                'code'  =>  9,
                'msg'   =>  '登录已过期!'   
            ]);
        }
        
        $request->uid = $uid;
        
        return $next($request);
    }
}
