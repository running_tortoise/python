<?php

namespace app\http\middleware;
use think\Db\Request;
use think\facade\Session;
use think\facade\Cache;

class apiMiddleware
{
    public function handle($request, \Closure $next)
    {
        
        // return json($request->header());
        
        $chientToken =  $request->header('token');
            
        $uid = $request->header('uid');
        
        $serverToken = Cache($uid."LOGIN_TOKEN");

        // return json([
        //     "chientToken"   =>  $chientToken,
        //     "uid"           =>  $uid,
        //     "serverToken"   =>  $serverToken,
        // ]);

        // if (Cache($uid."USER_AGENT")!=$_SERVER["HTTP_USER_AGENT"]){
        //     return response("{code:500,msg:非法请求1!}");
        // }
        
        // if ( !$serverToken ) return response("{code:500,msg:非法请求2!}");
        
        // //  验证token是否合法
        // if ( $serverToken!=$chientToken ){
        //     return response("{code:500,msg:token不合法!}");
        // }
        
        return $next($request);
    }
}
