<?php
/*
 * @Author: your name
 * @Date: 2020-12-30 16:41:00
 * @LastEditTime: 2021-10-25 15:35:27
 * @LastEditors: 奔跑的乌龟
 * @Description: In User Settings Edit
 * @FilePath: \waibaoe:\we\TPstudy\application\index\controller\Index.php
 */
namespace app\index\controller;
use think\Controller;
use think\Request;
use app\index\model\Books as Books;
use think\Db;
use think\facade\Cache;

class Book extends Controller
{
    //  分页获取最新书籍数据
    public function List($page=1)
    {
        $Books = new Books();
        $data = $Books->getBookList($page);
        return json(["code"=>200,"msg"=>"success","data"=>$data]);
    }
    
    //  获取单个书籍
    public function getBook($bid){

        if (!isset($bid)) return json(["code"=>500,"msg"=>"书籍ID错误!"]);
        $Book = (new Books())->getBook($bid);
        return json(["code"=>200,"msg"=>"success","data"=>$Book]);
    }

    //  收藏书籍
    public function callectbook(){
        $bid = input("get.bid");
        $uid = input("get.uid");
        //  插入之前先看一下是否已收藏
        $data = Db::name('collect')->where(["user_id"=>$uid, "book_id"=>$bid])->find();

        if ( !isset($data) ){
            $res = Db::name("collect")->insert([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid,
                "status"    =>  0,
                "addtime"  =>  time()
            ]);
            if ($res){
                return json(
                    [
                        "code"=>0,
                        "msg"=>"加入书架成功!",
                        "status"=>0
                    ]);
            }
        }else{
            if ($data["status"]==0){
                $res = Db::name("collect")->where([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid
                ])->update(["status"=>1]);
                if ( $res ){
                    return json(["code"=>0,"msg"=>"书籍已从书架中删除!","status"=>1]);
                }
            }else{
                $res = Db::name("collect")->where([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid
                ])->update(["status"=>0]);
                if ( $res ){
                    return json(["code"=>0,"msg"=>"添加到书架成功!","status"=>0]);
                }
            }
        }
    }

    /**
     * @likebook
     * @param $uid
     * @param $bid
     * @param $type=1为增加2为减少
     */
    public function likebook( $uid, $bid ){

        //  插入之前先看一下是否已收藏
        $data = Db::name('like')->where(["user_id"=>$uid, "book_id"=>$bid])->find();

        if ( !isset($data) ){
            $res = Db::name("like")->insert([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid,
                "status"    =>  0,
                "addtime"  =>  time()
            ]);
            if ($res){
                return json(["code"=>0,"msg"=>"加入喜欢成功!"]);
            }
        }else{
            if ($data["status"]==0){
                $res = Db::name("like")->where([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid
                ])->update(["status"=>1]);
                if ( $res ){
                    return json(["code"=>0,"msg"=>"成功移除喜欢列表!"]);
                }
            }else{
                $res = Db::name("like")->where([
                "user_id"   =>  $uid,
                "book_id"   =>  $bid
                ])->update(["status"=>0]);
                if ( $res ){
                    return json(["code"=>0,"msg"=>"加入喜欢成功!"]);
                }
            }
        }
    }
    
    
    /**
     * 获取用户对图书的状态
     * @param $uid
     * $param $bid
     */
    public function getUserBookStatus( $uid, $bid ){
         $data['code'] = 0;
        //  获取喜欢状态
        $likeStatus = Db::name('like')->where(["user_id"=>$uid, "book_id"=>$bid])->field('status')->find();
        $data['like'] = isset($likeStatus)?$likeStatus['status']:1;
        
        //  获取收藏状态
        $collectStatus = Db::name('collect')->where(["user_id"=>$uid, "book_id"=>$bid])->field('status,read_chapter')->find();
        
        $data['collect'] = isset($collectStatus)?$collectStatus['status']:1;
        
        $bookFirstCapter = Db::name('chapter')->where('book_id',$bid)->field('id')->find();
        $data['read_chapter'] = isset($collectStatus)?$collectStatus['read_chapter']:$bookFirstCapter['book_id'];
        if($collectStatus['read_chapter']==0) $data['read_chapter'] = $bookFirstCapter['id'];
        
        
        return json($data);
    }
    
    
     /**
     * 模糊搜索图书
     * @param $name
     */
    public function searchBook( $name ){
        
        // 根据图书和作者名搜索
        $data = Db::name('books')->where('title|author','like',"%$name%")->select();
        
        return json([
                'code'  =>  0,
                'msg'   =>  'ok',
                'data'  =>  $data
            ]);
    }
    
    
    /**
     * 获取收藏列表
     * @param $uid
     */
    public function getCollect( $uid ){
        // 根据图书和作者名搜索
        $data = Db::name('collect')->where(['user_id'=>$uid,'status'=>0])->field('book_id')->select();
        $res = [];
        foreach ($data as $index){
            $book = Db::name('books')->where('book_id',$index['book_id'])->field('Id,book_id,photo,title')->find();
            array_push($res,$book);
        }
        return json([
                'code'  =>  0,
                'msg'   =>  'ok',
                'data'  =>  $res
        ]);
    }
     
     
    /**
     * 搜索收藏的图书
     * @param $uid
     */
    public function searchCollectBook( $uid, $name ){
        // 根据图书和作者名搜索
        $data = Db::name('collect')->where(['user_id'=>$uid])->field('book_id')->select();
        $res = [];
        foreach ($data as $index){
            $book = Db::name('books')->where('book_id',$index['book_id'])->where('title|author','like',"%$name%")->field('Id,book_id,photo,title')->find();
            if (isset($book)){
                array_push($res,$book);
            }
        }
        return json([
                'code'  =>  0,
                'msg'   =>  'ok',
                'data'  =>  $res
        ]);
    } 
     
    /**
     * 获取图书分类
     *  */ 
    public function getClassify(){
        $data = Db::name('classification')->select();
        for ($i=0; $i < count($data); $i++) { 
            $sum = Db::name('books')->where('type', $data[$i]['id'])->count();
            $data[$i]['sum'] = $sum?$sum:0;
        }
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
     
     
    /**
    * @通过分类查询图书
    * @param $tid 
    */ 
    public function getClassBook($tid){
        $data = (new Books)->getClassBook($tid);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    
    /**
    * @榜单
    */
    public function topBooks($page){
        $data = (new Books)->getTopTenBooks($page);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    /*
    * 获取热门分类 number 5
    */
    public function getHotClassify(){
        $data = (new Books)->getHotClassify();
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    /*
    * 获取热门分类 number 5
    */
    public function newBooks($page=1){
        $data = (new Books)->getBookList($page,5);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    /*
    * 获取月榜单
    */
    public function monthTopBook($page){
        $month = time()-86400*30;
        $data = (new Books)->geTopBooks($page, 10, $month);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    
    /*
    * 获取周榜单,最新热门
    */
    public function weekTopBook($page){
        $week = time()-(86400*7);
        $data = (new Books)->geTopBooks($page, 5, $week);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    
    /*
    * 获取周榜单,最新热门
    */
    public function topLikeBook($page=1){
        $data = (new Books)->getLikeTop($page);
        return json([
            'code'  =>  0,
            'msg'   =>  'ok',
            'data'  =>  $data
        ]);
    }
    
    
    
    
    
    
    
    
    
    
}
