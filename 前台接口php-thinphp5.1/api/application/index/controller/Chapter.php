<?php
/*
 * @Author: your name
 * @Date: 2020-12-30 16:41:00
 * @LastEditTime: 2020-12-30 21:46:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \waibaoe:\we\TPstudy\application\index\controller\Chapter.php
 */
namespace app\index\controller;
use think\Controller;
use think\Facade\Request;
use app\index\model\Books as Books;
use think\Db;

class Chapter extends Controller
{
    //  获取章节列表
    public function getchapter($bid){
        $data = Db::name("chapter")->where('book_id',$bid)->field("Id,chaptertitle")->select();
        return json([
            "code"  =>  0,
            "msg"   =>  "获取成功",
            "data"  =>  $data
        ]);
    }
    
    
    //  获取章节内容
    public function getChapterContent($cid){
        $data = Db::name("chapter")->where('id',$cid)->field("content")->find();
        return json([
            "code"  =>  0,
            "msg"   =>  "获取章节内容成功",
            "data"  =>  $data
        ]);
    }
}
