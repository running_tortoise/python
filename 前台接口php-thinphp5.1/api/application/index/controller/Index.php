<?php
/*
 * @Author: your name
 * @Date: 2020-12-30 16:41:00
 * @LastEditTime: 2021-10-17 11:55:47
 * @LastEditors: 奔跑的乌龟
 * @Description: In User Settings Edit
 * @FilePath: \waibaoe:\we\TPstudy\application\index\controller\Index.php
 */
namespace app\index\controller;
use think\Controller;
use think\Facade\Request;
use app\index\model\User as Users;
use think\Db;
use think\facade\Log;
use Redis;

class Index extends Controller
{


    public function index(){
        return view();
        
    }

    /**
    * @param register
    * @var username, email, password, ip
    * @return json
    */
    public function register(){

        $data = request()->put();
        $Users = new Users();

        if ($Users->isUser($data["email"])>0) return json(["code"=>202,"msg"=>"该用户已存在!"]);
        if ($Users->addUser($data["email"],$data["password"],$data["ip"])>0){
            return json(["code"=>200,"msg"=>"注册成功!","data"=>request()->put()]);
        }else{
            return json(["code"=>500,"msg"=>"注册失败!"]);
        }
    }
    
    /**
     * @param login
     * @var email, password ,ip
     * @return json
     */
    public function login(){
        
        $data = request()->put();
        $Users = new Users();
        
        if ($Users->isUser($data["email"])<1) return json(["code"=>404,"msg"=>"该用户不存在!"]); 
        $res = $Users->login($data["email"],$data["password"],$data["ip"]);
        
        if ($res){
            $token = $Users->generateToken($res["user_id"],[$res["user_id"],$res["email"]]);
            return json(["code"=>200,"msg"=>"登陆成功!","data"=>$res,"token"=>$token]);
        }else{
            return json(["code"=>500,"msg"=>"密码错误!"]);
        }
    }
}
