<?php
/*
 * @奔跑的乌龟
 * @Date: 2020-12-30 16:41:01
 * @LastEditTime: 2020-12-31 22:06:54
 */
namespace app\index\model;

use think\Db;
use think\Model;
use think\facade\Session;

class Books extends Model
{
    
    //  分页获取书籍数据
    public function getBookList($page,$num=10){
        $data = Books::page($page,$num)->order('id', 'desc')->select();
        return $data;
    }
    
    //  根据书籍Id查询书籍
    public function getBook($bid){
        $book = Books::where(["Id"=>$bid])->find();
        // $good["file_path"] =  $this->getGoodAllPhoto($bid);
        return $book;
        
    }
    
    //  查询分类图书
    public function getClassBook($class_id){
        return Books::where('type', $class_id)->select();
    }
    
    
    //  查询榜单书籍
    public function getTopTenBooks($page){
        // $sql = "SELECT book_id, COUNT(book_id) FROM novel_collect GROUP BY book_id ORDER by COUNT(book_id) desc LIMIT 10";
        
        $sql = "SELECT b.*,c.book_id, COUNT(c.book_id) FROM novel_collect as c, novel_books as b where c.book_id=b.book_id GROUP BY c.book_id ORDER by COUNT(c.book_id) desc LIMIT " .$page. ",10";
        
        $res = Db::query($sql);
        return $res;
    }
    
    //  获取热门分类
    public function getHotClassify(){
        $sql = "SELECT cl.id,cl.name, b.type FROM novel_collect as c
        INNER JOIN novel_books as b
        on c.book_id=b.book_id
        inner join novel_classification as cl
        where b.type=cl.id
        GROUP BY b.type
        ORDER BY count(b.type) DESC
        LIMIT 5";
        
        $res = Db::query($sql);
        
        for ($i = 0; $i < count($res); $i++) {
            $sum = Db::name("books")->where("type", $res[$i]["type"])->count();
            $res[$i]["sum"] = $sum;
        }
        return $res;
    }
    
    //  根据时间查询榜单
    public function geTopBooks($page, $num=10, $time=878448813){

        $sql = "SELECT b.*,c.book_id, c.addtime, COUNT(c.book_id) FROM novel_collect as c, novel_books as b where c.book_id=b.book_id and c.addtime>= ".$time." GROUP BY c.book_id ORDER by COUNT(c.book_id) desc LIMIT " .$page*$num. ", ".$num;
        
        $res = Db::query($sql);
        return $res;
    }
    
     //  喜欢排行榜
    public function getLikeTop($page){
        $sql = "SELECT b.book_id, COUNT(l.book_id),b.author,b.Id, b.title,b.photo,b.synopsis FROM novel_like as l
        INNER JOIN novel_books as b
        WHERE l.book_id=b.book_id
        GROUP BY l.book_id
        ORDER BY COUNT(l.book_id) DESC
        LIMIT ".($page*3)." ,3";
        
        $res = Db::query($sql);
        return $res;
    
    }
    
    
    
    
    
    
    
    
    
    
    
    
}