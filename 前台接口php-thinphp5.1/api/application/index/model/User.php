<?php
/*
 * @奔跑的乌龟
 * @Date: 2020-12-30 16:41:01
 * @LastEditTime: 2020-12-31 22:06:54
 */
namespace app\index\model;

use think\Db;
use think\Model;
use think\facade\Session;
use think\facade\Cache;

class User extends Model
{
    /**
     * @param 用户id 
     */
    protected $user_id; 

    /**
     * @param 邮箱
    */
    protected $email;

    /**
     * @param 用户名
    */
    protected $username;

    /**
     * @param 手机号
    */
    protected $mobile;

    /**
     * @param 密码
    */
    protected $password;

    /**
     * @param 添加用户
     * @var $mobile,$mobile,$password,$ip
     */
    public function addUser($mobile,$password,$ip){
        $user = new User([
            "username"  => $mobile,
            "mobile"  => $mobile,
            "password"  => md5(Config("app","salt").$password),
            "register_time"   => time(),
            "register_ip" => $ip
        ]);
        return $user->save();
    }

    //  判断用户是否已存在
    public function isUser($mobile){
        return User::where(["mobile"=>$mobile])->count();
    }

    //  查询单个用户
    public function getUser($mobile){
        return Users::where(["mobile"=>$mobile])->find();
    }

    //  生成token并加入缓存
    public function generateToken($uid,array $data){
        $token = base64_encode(md5(time())).md5(uniqid());
        foreach($data as $value){
            $token = $token.base64_encode(md5($value));
        }
        Cache($uid."LOGIN_TOKEN",$token);
        Cache($uid."USER_AGENT",$_SERVER["HTTP_USER_AGENT"]);
        return $token;
    }

    //  H5用户登陆
    public function login($mobile,$password,$ip){
        $pwd = User::where(["mobile"=>$mobile])->value("password");
        if ( $pwd==md5(Config("app", "salt").$password) ){
            
            $userinfo = User::where(["mobile"=>$mobile])->field("user_id,mobile,username,head,login_time")->find();
            //  更新登陆信息
            User::where(["mobile"=>$mobile])->update([
                    "login_time" => time(),
                    "ip" => $ip
                ]);
            $userinfo["login_time"] = date("Y-m-d H:i:s",time());
            return $userinfo;
        }else{
            return false;
        }
    }
    
    //  微信小程序用户登陆
    public function wxlogin($wid, $nickname, $head){
        
        //  如果存在用户
        $userinfo = User::where(["wx_openid"=>$wid])->field("user_id,mobile,username,head,login_time")->find();
        if ($userinfo){
            //  更新登陆信息
            User::where(["wx_openid"=>$wid])->update([
                    "login_time" => time(),
            ]);
            $userinfo["login_time"] = date("Y-m-d H:i:s",time());
            return $userinfo;
        }
        
        // 如果不存在则进行注册
        $uid = $this->wxRegister($wid, $nickname, $head);
        if ($uid>0){
            return User::where(["wx_openid"=>$wid])->field("user_id,mobile,username,head,login_time")->find();
        }else{
            return false;
        }
        
    }
    
    
    
    //  更新用户单个字段信息
    public function updateField($uid,$field,$value){
        return User::where(["user_id"=>$uid])->setField($field,$value);
    }
    
    
    //  微信小程序注册
    public function wxRegister($wid, $nickname, $head){
        return User::insertGetId([
                "wx_openid" => $wid,
                "username"  => $nickname,
                "head"      => $head,
                "register_time"   => time()
            ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}