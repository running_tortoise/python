<?php
namespace app\common\exception;

use Exception;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\ValidateException;

class Http extends Handle
{
    public function render(Exception $e)
    {
        $deBug =  true;
        //  查看当前是否开启deBug模式
        if ($deBug){
            return json([
                "code"  => 500,
                "message"   =>"内部服务器错误",
                "info"  =>  [
                    //  文件
                    "errorFile" => $e->getFile(),
                    //  行号
                    "errorLine" => $e->getLine(),
                    //  错误描述
                    "errorMessage"=>$e->getMessage(),
                    //  追踪信息
                    "errorCode" => $e->getCode(),
                ]
            ]);
        }

        // 参数验证错误
        if ($e instanceof ValidateException) {
            return json(["code"=>500,"msg"=>"缺少必要参数"]);
        }

        // 请求异常
        if ($e instanceof HttpException && request()->isAjax()) {
            return json(["code"=>500,"msg"=>"请求异常!"]);
        }

        // 其他错误交给系统处理
        return parent::render($e);
        // return json(["code"=>500,"msg"=>"api error!"]);
    }

}