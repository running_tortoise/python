'''
Author: 奔跑的乌龟
email: 435327238@qq.com
'''
import pymysql
import traceback
import sys
import log
class DbController:
    

    host = 'localhost'
    username = 'root'
    password = 'root'
    database = 'database'
    port = 3306
    conn = {}
    transactionFlag = False
    isException = False

    '''
     @构造函数
     @param sql
    '''
    def __init__(self) -> None:
        # 打开数据库链接
        self.conn = pymysql.connect (host=self.host, user=self.username, password=self.password, database=self.database )
        if self.conn.open == False:
            quit("数据库连接失败")

    '''
        @查询数据
        @param sql
    '''
    def query(self, sql):
        # 创建游标对象
        cursor = self.conn.cursor()
        # 执行sql语句
        cursor.execute(sql)
        data = cursor.fetchall()
        # 提交数据库执行
        self.conn.commit()
        # 关闭数据库连接
        self.conn.close()
        return data

    '''
        @插入数据
        @param string sql
        @param array data
    '''
    def insert(self, sql, data):
        try:
            # 创建游标对象
            cursor = self.conn.cursor()
            # 执行sql语句
            cursor.execute(sql, data)
        except BaseException as e:
            # 错误文件
            errFile = e.__traceback__.tb_frame.f_globals['__file__']
            # 错误行号
            errLine = e.__traceback__.tb_lineno
            a, b, c = sys.exc_info()
            log.writeLog(errFile, errLine, a, b)

            # 如果开启事务
            if (self.transactionFlag):
                print("单条数据开启了事务")
                self.isException = True
            else:
                self.conn.rollback()
            print("插入单条数据失败!")
        finally:
            if (self.transactionFlag==False):
                self.conn.commit()
                # 关闭连接
                self.conn.close()

        
    def inserts(self, sql, datas):
        try:
            # 创建游标对象
            cursor = self.conn.cursor()
            # 执行sql语句
            cursor.executemany(sql, datas)
        except BaseException as e:
            # 错误文件
            errFile = e.__traceback__.tb_frame.f_globals['__file__']
            # 错误行号
            errLine = e.__traceback__.tb_lineno
            a, b, c = sys.exc_info()
            log.writeLog(errFile, errLine, a, b)

            # 如果开启事务
            if (self.transactionFlag):
                print("多条数据开启了事务")
                self.isException = True
            else:
                self.conn.rollback()
            print("插入多条数据失败!")
        finally:
            if (self.transactionFlag==False):
                self.conn.commit()
                # 关闭连接
                self.conn.close()

    '''
        @修改数据
        @param string sql
        @param array data
    '''
    def update(self, sql):
        try:
            # 创建游标对象
            cursor = self.conn.cursor()
            # 执行sql语句
            cursor.execute(sql)
        except BaseException as e:
            # 错误文件
            errFile = e.__traceback__.tb_frame.f_globals['__file__']
            # 错误行号
            errLine = e.__traceback__.tb_lineno
            a, b, c = sys.exc_info()
            log.writeLog(errFile, errLine, a, b)
            
            # 如果开启事务
            if (self.transactionFlag):
                print("修改数据开启了事务")
                self.isException = True
            else:
                self.conn.rollback()
            print("修改数据失败!")
        finally:
            if (self.transactionFlag==False):
                self.conn.commit()
                # 关闭连接
                self.conn.close()



    # 事务开始
    def transaction_start(self):
        print("事务开始!")
        self.transactionFlag = True
    

    # 事务结束
    def transaction_end(self):
        if self.isException:
            self.conn.rollback()
        else:
            self.conn.commit()
        
        self.conn.close()
        print("事务结束!")