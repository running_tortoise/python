import logging
import datetime
import time

# logging.basicConfig(filename='./runtime/'+str(datetime.datetime.now().month)+"-"+str(datetime.datetime.now().day)+'.log', encoding='utf-8', level=logging.ERROR)
logging.basicConfig(filename='./runtime/28'+'.log',level=logging.ERROR)
def writeLog(file, line, type, msg, info=""):
    logging.error({
        "file":file,
        "line":line,
        "type":type,
        "msg":msg,
        "info":info,
        "datetime": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    })


if __name__ == "__main__":
    print("打印错误日志!")
