'''
Author: 奔跑的乌龟
email: 435327238@qq.com
'''

from pymysql import DATE
from Request import Request
from DbController import DbController
from lxml import etree
import log
import sys


class Chapter:
    # 链接
    url = 'http://www.aidusk.com/t/'

    # 书籍id
    book_id = ''

    # 章节名
    title = ''

    # 文章内容
    content = ''

    # 爬取列表
    taskList = []
    
    # 数据列表
    datas = []

    # 文档
    response_Text = ''


    # 构造函数
    def __init__(self, id, url):
        self.url = url
        self.book_id = id
        Req = Request(self.url)
        self.response_Text = etree.HTML(Req.get())

    # 解析页面
    def parseData(self):
        try:
            # 书名
            self.title = self.response_Text.xpath('//div[@class="date"]/h1/text()')[0]
            # 章节内容
            content = self.response_Text.xpath('//div[@id="content"]/text()')
            self.content = ''
            for val in content:
                self.content += str(val)
            
            # 给数据复制
            self.datas = [self.book_id, self.title, self.content]
            
        except BaseException as e:
            # 错误文件
            errFile = e.__traceback__.tb_frame.f_globals['__file__']
            # 错误行号
            errLine = e.__traceback__.tb_lineno
            a, b, c = sys.exc_info()
            log.writeLog(errFile, errLine, a, b)
            return []
        
        return self.datas

