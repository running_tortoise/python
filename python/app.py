import sys
from os import system
from DbController import DbController
from Book import Book
import time
import traceback
import log

# 获取书籍
def getBook(num):
    # 先查询已有数据的最大书籍id
    Db = DbController()
    sql = "select max(book_id) from novel_books"
    res =  Db.query(sql)
    maxId = res[0][0] if str(res[0][0])!="None" else 0
    time.sleep(1)
    print("程序将在3秒后开始执行,本次爬取会为您爬取"+str(num)+"条数据,您也可以使用Ctrl+C来提前结束")
    # time.sleep(3)
    print("---------------start--------------")
    while(num>0):
        num -= 1
        maxId += 1
        try:
            Book(maxId)
        except BaseException as e:
            print("本次数据出错,错误信息是：")
            print(str(e))
            # 错误文件
            errFile = e.__traceback__.tb_frame.f_globals['__file__']
            # 错误行号
            errLine = e.__traceback__.tb_lineno
            a, b, c = sys.exc_info()
            log.writeLog(errFile, errLine, a, b)
            continue
        finally:
            print("---------------end--------------")

if __name__ == "__main__":
    getBook(99999)
