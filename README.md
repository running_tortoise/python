# 部署文档

## 一、部署环境

### 1.数据库

数据库类型：mysql

版本：5.7

**安装方式***

导入testtest.sql文件即可

### 2.数据采集端

运行系统：linux|windows

语言：python3

包管理工具：pip

依赖模块：

```text
bs4==0.0.1
lxml==4.6.3
PyMySQL==1.0.2
requests==2.25.1
```

**安装方式：**

安装好python3和pip环境后，在项目主目录终端下依次执行

```pip
pip install bs4==0.0.1

pip install lxml==4.6.3

pip install PyMySQL==1.0.2
	
pip install requests==2.25.1
```

也可使用自动安装方式：该方式依赖 requirements.txt 文件

```pip
pip install -r requirements.txt
```



### 3.前端接口

运行系统：linux|windows

语言：php

版本：7.0^

框架：thinkphp

网站环境：nginx|apache

确认环境安装好后：

​	将前台接口php-thinphp5.1放到网站主目录下，将运行目录改为php-thinphp5.1\public

​	设置伪静态：

**nginx**

```nginx
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}
```

**apache**

```apache
<IfModule mod_rewrite.c>
  Options +FollowSymlinks -Multiviews
  RewriteEngine On

  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
</IfModule>

```

访问http://域名    如果内容为{"code":404,"msg":"api not found!"}，即代表部署成功。



### 4.前端代码

技术栈：uniapp+vue

运行环境：Hbuilder3.29^

将**前端uniapp-h5+微信小程序**导入到Hbuilder中，点击发行，选择**H5**或**微信小程序打包即可**

